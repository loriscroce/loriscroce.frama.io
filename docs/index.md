---
hide:
    - toc
title: Accueil
---

# Poste actuel

*Ingénieur d'études* à la [MSH](https://msh.uca.fr) : humanités numériques et développement web.

# Précedemment

- *Ingénieur d'études* dans l'unité [INRAE TSCF](https://tscf.clermont.hub.inrae.fr/.). Associé au projet [ANR CoSWoT](https://coswot.gitlab.io/) avec Gil De Sousa dans le WP4 : Cas d'usages, simulations, expérimentations.
- *Ingénieur d'études* au [LIMOS](https://limos.fr) et à [Jeolis Solutions](https://www.lojelis.com/us/research-and-development/)
 dans le cadre de [France Relance R&D](https://anr.fr/fr/plan-de-relance/) pour le projet [Exialis](https://limos.fr/news_project/283) : fournir une e-ETO basé sur le meilleur des deux mondes d'OWL et ASP.
- *Ingénieur de recherche* dans l'unité [LISC INRAE](https://lisc.inrae.fr) sur le projet 
[Agriterix](https://forgemia.inra.fr/agriterix/simulator) dans le cadre du challenge 1 de l'I-SITE CAP-2025 : [Agrosystèmes durables dans un contexte de changement global](https://cap2025.fr/challenges-scientifiques/les-agroecosystemes-durables-dans-un-contexte-de-changement-global/challenge-1-agro-ecosystemes-durables-dans-un-contexte-de-changement-global). Développement d'un modèle pour la transition agricole au niveau des parcelles avec Nicolas Dumoulin et Franck Jabot.
-   *Stagiaire* sur le projet [e-DOL](https://www.institut-analgesia.org/portfolio-item/projet-edol/)
    de l'Institut Analgesia et [e-Santé, Mobilité, Big-DATA](https://esante-mobilite.limos.fr/) du LIMOS. Lors de ce stage supervisé par Jean-Marie Favreau, j'ai commencé à développer un pipeline pour enregistrer et stocker les données des capteurs à haute fréquence ([application finale](https://gitlab.limos.fr/esante-mobilite/esante-app/)).
-   *Stagiaire* sur le projet [Multipass](https://numerique.acta.asso.fr/multipass/) dans l'unité [TSCF INRAE](https://tscf/clermont.hub.inrae.fr).
    Durant ce stage avec Laetitia Lemière encadré par François Pinet . J'ai travaillé sur l'anonymisation des données géo-référencées (publication : [Une nouvelle méthodologie pour l'anonymisation des entrepôts de données spatiales : application aux données de biodiversité dans le contexte agricole](https://editions-rnti.fr/?inprocid=1002543)).

# Enseignement

|    Année     |           Sujet           |             Groupe             | Localisation | Descriptif                                                                 |
|:------------:|:-------------------------:|:------------------------------:|:-------------|:---------------------------------------------------------------------------|
|     2025     | Développement Web serveur |    Informatique 2ème année     | UCA          | [support](Enseignement/WebServer_L2.md)                                    |
|     2024     | Internet des objets (IoT) |    Informatique 5ème année     | Hésias       | Principes, cas d'utilisation, technologies [supports](Enseignement/IoT.md) |
| 2024 & 2025  |      Génie logiciel       |    Informatique 3ème année     | UCA          | UML, modèles de conception, git, etc.                                      |
|     2023     |   Programmation avancée   |    Informatique 2ème année     | UCA          | Programmation C : structures, pointeurs etc.                               |
| 2021 et 2024 | Développement Web serveur |    Informatique 2ème année     | UCA          | Java Spark, FreeMarker                                                     |
|  2021-2023   | Développement Web client  | Informatique/MIASHS 2ème année | UCA          | HTML/CSS/Javascript, jQuery                                                |
|     2021     |     Bases de données      |       MIASHS 2ème année        | UCA          | SQLite, Algèbre Relationnelle                                              |

# Formations

- Master en Informatique à l'[ISIMA,
UCA](https://www.uca.fr/formation/nos-formations/catalogue-des-formations/master-info)
en génie logiciel.
- Licence en Informatique à l'[ISIMA,
UCA](https://www.uca.fr/formation/nos-formations/catalogue-des-formations/master-info)
- DUT MMI à [IUT d'Allier](https://www.uca.fr/formation/nos-formations/catalogue-des-formations/but-metiers-du-multimedia-et-de-linternet-vichy)

# Contact

-   professionnel: `loris[point]croce[arobase]uca[point]fr`
-   personnel: `loris[point]croce[arobase]laposte[point]net`

---------------------------------------------------

[CV](cv.pdf){ .md-button .md-button--primary}

