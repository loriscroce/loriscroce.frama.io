# Outils utilisés

## Bureau

- [Gnome](https://www.gnome.org/)

## Éditeur

- [VSCodium](https://vscodium.com/)
- [micro](https://micro-editor.github.io/)

## Extensions 

### Gnome

- [Forge](https://github.com/forge-ext/forge)
- [Vitals](https://extensions.gnome.org/extension/1460/vitals/)

### Firefox

- [Dark Reader](https://darkreader.org/)
- [uBlock Origin](https://github.com/gorhill/uBlock#ublock-origin)
- [Ghostery](http://www.ghostery.com/)
- [SponsorBlock](https://sponsor.ajay.app/)
- [Blue Blocker](https://github.com/kheina-com/Blue-Blocker)
- [redditUnstranslate](https://github.com/SeidSmatti/redditUntranslate)
- [Sci-Hub](https://addons.mozilla.org/fr/firefox/addon/sci-hub-addon/?utm_content=addons-manager-reviews-link&utm_medium=firefox-browser&utm_source=firefox-browser)
- [Native MathML](https://github.com/fred-wang/webextension-native-mathml)
- [Mastodon - Fédération simplifiée](https://github.com/rugk/mastodon-simplified-federation)
- [Simple Tab Groups](https://github.com/drive4ik/simple-tab-groups)

## Misc

- [Quickemu](https://github.com/quickemu-project/quickemu)
- [Typst](https://typst.app/)
- [Pandoc](https://pandoc.org/)
- [ImageMagick](https://imagemagick.org/index.php)
- [FFmpeg](https://www.ffmpeg.org/)
- [Signature PDF](https://github.com/24eme/signaturepdf)
- [Home Assistant](https://www.home-assistant.io/)
- [Valetudo](https://valetudo.cloud/)
- [PlatformIO](https://platformio.org/)

...