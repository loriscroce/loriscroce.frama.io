---
hide:
    - toc
title: Home
---

# Current position

*Software Engineer* at [MSH](https://msh.uca.fr/): digital humanities and web development.

# Previous

- *Software engineer* at [INRAE TSCF](https://www6.clermont.inrae.fr/tscf/) on the [CoSWoT ANR Project](https://coswot.gitlab.io/) with Gil De Sousa in WP4: Use cases, Simulations, Experiments.
- *Software engineer* at [LIMOS](https://limos.fr) and [Jeolis Solutions](https://www.lojelis.com/us/research-and-development/)
 with [ANR R&D renewal plan](https://anr.fr/fr/plan-de-relance/). For the [Exialis project](https://limos.fr/news_project/283): provide an e-TPE based on best of both world of OWL and ASP.
- *Research engineer* at [INRAE LISC](https://lisc.inrae.fr) on the
[Agriterix](https://forgemia.inra.fr/agriterix/simulator) project, a
model of agricultural transition at the landscape scale with Nicolas
Dumoulin and Franck Jabot. This project is part of the challenge:
[Sustainable agroecosystems in a context of global
change](https://cap2025.fr/english-version/research/scientific-challenges/sustainable-agroecosystems-in-a-context-of-global-change)
of I-SITE Clermont Auvergne Project.
-   *Intern* on the [eDOL
    project](https://www.institut-analgesia.org/portfolio-item/projet-edol/)
    by Analgesia Institute and [e-Health, Mobility and Big
    Data](https://esante-mobilite.limos.fr/en/) by LIMOS. During this
    internship supervised by Jean-Marie Favreau, I started to develop a
    pipeline to record and store sensors data at high frequency
    ([smarthone
    app](https://gitlab.limos.fr/esante-mobilite/esante-app/)).
-   *Intern* on the [Multipass
    project](https://numerique.acta.asso.fr/multipass/) at [Copain TSCF
    INRAE](https://www6.clermont.inrae.fr/tscf/Les-equipes/Equipe-Copain).
    During this internship supervised by François Pinet with Laetitia
    Lemiere. I worked on agricultural data anonymisation, this led to a
    publication : [A new methodology for anonymizing spatial data
    warehouses: application to biodiversity data in the agricultural
    context](https://editions-rnti.fr/?inprocid=1002543) (in french).

# Teaching (tutorials)

|    Year     |           Subject           |        Group         | Location | Description                              |
|:-----------:|:---------------------------:|:--------------------:|:---------|:-----------------------------------------|
|    2025     | Server side web development |     CS 2nd year      | UCA      | [here](Enseignement/WebServer_L2.md)     |
|    2024     |  Internet of Things (IoT)   |     CS 5th year      | Hesias   | [here](Enseignement/IoT.md)              |
| 2024 & 2025 |    Software engineering     |     CS 3rd year      | UCA      | UML, Design patters, git, etc.           |
|    2023     |    Advanced Programming     |     CS 2nd year      | UCA      | C programming: structures, pointers etc. |
| 2021 & 2024 | Server side web development |     CS 2nd year      | UCA      | Java Spark, FreeMarker                   |
|  2021-2023  | Client side web development | CS / MIASHS 2nd year | UCA      | HTML/CSS/Javascript, jQuery              |
|    2021     |          Databases          |   MIASHS 2nd year    | UCA      | Sqlite, Relationnal Algebra              |

# Education

- Master's Degree in Computer Science at [ISIMA,
UCA](https://www.uca.fr/formation/nos-formations/catalogue-des-formations/master-info)
in software engineering.
- Bachelor's Degree in Computer Sience at [ISIMA,
UCA](https://www.uca.fr/formation/nos-formations/catalogue-des-formations/master-info)
- DUT MMI at [IUT d'Allier](https://www.uca.fr/formation/nos-formations/catalogue-des-formations/but-metiers-du-multimedia-et-de-linternet-vichy)

# Contact me

-   professional: `loris[dot]croce[at]uca[dot]fr`
-   personal: `loris[dot]croce[at]laposte[dot]net`

------------------------------------------------------------------------

[Resume](cv_en.pdf){ .md-button .md-button--primary }

