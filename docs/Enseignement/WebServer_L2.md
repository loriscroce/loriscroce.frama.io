# Web Serveur - L2

> Votre client, après avoir trouvé beaucoup trop de (mauvaises) excuses pour la gestion chaotique de son organisation, a décidé de faire appel à vous pour développer une application web collaborative de gestion de tâches. Des discussions interminables avec lui ont permis d'établir un cahier des charges.

## Cahier des charges

- Une tâche comporte un **titre**, , un **statut** *(accomplie ou à faire)* et peut avoir une _**description**_, une _**liste de sous-tâches**_,  _**une liste de personnes**_ et _**une liste d'équipes rattachées**_.
- L'application doit être **multi-utilisateurs** et supporter *l'authentification* et *l'enregistrement*.
- Un-e utilisateur/trice est une personne qui a un **nom**, peut avoir une _**description**_ et une _**équipe**_. Elle s'authentifie avec son **e-mail** et son **mot de passe**.
- Une équipe a un **nom** et une **liste de personnes** membres de l'équipe, elle peut être *créée*, *rejointe* ou *quittée* par une personne.
- Une personne peut *ajouter*, *modifier* ou *supprimer* une tâche.
- Une tâche *peut-être assignée* à *une ou plusieurs personnes/équipe*, elle peut également être *désassignée* ou *réassignée*.
- Une tâche peut être *privée* : visible seulement par la personne qui a créé la tâche et les personnes/équipes qui y ont été associées ; ou *publique* : visible par tout le monde.
- Une tâche peut faire **référence à une autre tâche** dans sa description.
- Une personne peut voir le profil d'une autre personne/équipe ainsi que les tâches publiques qui lui ont été assignées.

## Quelques conseils

### Avant de se mettre à développer

- Lister et détailler les entités dont vous allez avoir besoin.
- Établisser routes de votre application.
- Préparer les différentes vues de votre application.

### Outils

Il existe plusieurs framework MVC en fonction de votre préférence de language, quelques propositions :

- [Django](https://www.djangoproject.com/) pour Python ([documentation](https://docs.djangoproject.com/en/5.1/))
- [Spring](https://spring.io/) pour Java (guides : [contenus web](https://spring.io/guides/gs/serving-web-content), [authentification](https://spring.io/guides/gs/securing-web), [documentation](https://docs.spring.io/spring-framework/reference/))
- [Symfony](https://symfony.com) pour PHP ([documentation](https://symfony.com/doc/current/setup.html))

C'est également une bonne idée d'utiliser git, d'autant plus que vous pouvez vous faire un compte sur l'instance [Gitlab de l'ISIMA](https://gitlab.isima.fr). Cela vous permet de travailler plus facilement en groupe et de sauvegarder votre travail.

### Autre

- Lisez bien le sujet et n'hésitez pas à me poser des questions ;
- Il n'y a pas qu'une solution d'implémentation pour ce TP ;
- Je préfère corriger votre code que celui de ChatGPT et ses homologues.

## Modalités de rendus et d'évaluation

    TODO

<!-- - Le TP se fait en binôme du même groupe. Cela veut dire :
    - pas de trinômes ou plus ;
    - pas de binôme avec un autre groupe ;
    - Dans le cas exceptionnel de groupe au nombre  -->